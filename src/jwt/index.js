import jwt from 'jsonwebtoken';
import fs from 'fs';

//TODO: где-то не здесь хранится приватный ключ:
const privateKey = fs.readFileSync('./jwt_private.key', 'utf8');

const publicKey = fs.readFileSync('./jwt_public.key', 'utf8');

const issuer = 'Some issuer';
const audience = 'some-bull-api.kz';
const expiresIn = '10000h';//TODO: Должен быть коротким
const algorithm = 'RS256';

//https://tools.ietf.org/html/rfc7519#section-4.1
//https://github.com/auth0/node-jsonwebtoken
const signOptions = {
    issuer,
    //TODO: subject -> sub на сервере аутентификации
    audience,
    expiresIn,
    algorithm
};

//TODO: где-то не здесь пользователь проходит аутентификацию, получая токен:
export const sign = function(payload) {
    return jwt.sign(payload, privateKey, signOptions)
}

const verifyOptions = {
    issuer,    
    audience,
    maxAge: expiresIn,
    algorithms: [algorithm]
};

export const verify = function(token) {
    return jwt.verify(token, publicKey, verifyOptions);
}